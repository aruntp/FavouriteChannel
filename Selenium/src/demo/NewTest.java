package demo;

import org.jboss.netty.handler.codec.http.HttpHeaders.Values;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;
import java.util.List;

public class NewTest {

  public static final String USERNAME = "sanjay348";
  public static final String AUTOMATE_KEY = "qHxCYx5fMx3ZTHbBheTo";
  public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

  public static void main(String[] args) throws Exception {

    DesiredCapabilities caps = new DesiredCapabilities();
    caps.setCapability("browser", "firefox");
    caps.setCapability("browserstack.debug", "true");
    caps.setCapability("build", "Proximus");
    System.out.println("step1");
    WebDriver driver = new RemoteWebDriver(new URL(URL), caps);
    driver.get("http://www.proximustv.be/fr");
    driver.manage().window().maximize();
    Thread.sleep(2000);
    System.out.println("step2");
    // To select a Programme TV using xpath
    driver.findElement(By.xpath(".//*[@id='header-slider']/ul/li[2]/a")).click();
    Thread.sleep(5000);
    System.out.println("aaaaa");
    
    // To select one programme from epg-grid
    WebElement sel = driver.findElement(By.className("pxepg-row"));
    List<WebElement> list1 = sel.findElements(By.className("pxepg__main-container"));
    for(WebElement lt: list1){
    	System.out.println(lt.getText());
    	break;
    }
//    WebElement sel = driver.findElement(By.className("pxepg-row"));
//    List<WebElement> list = sel.findElements(By.className("pxepg__main-container"));
//    for(WebElement lt: list){
//    	System.out.println("uuuu");
//    	if(lt.getText().equals("Power Rangers: Dino Charge")){
//    		System.out.println("selecting Program");
//    		System.out.println("true");
//    		lt.click();
//    		break;
//    	}
//    	System.out.println("false");
//    }
    System.out.println("bbbbb");
    Thread.sleep(10000);
   
//    System.out.println(driver.getTitle());
//    System.out.println("DONE !!!!");
    driver.quit();
  }
}

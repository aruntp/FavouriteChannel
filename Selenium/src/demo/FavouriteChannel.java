package demo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import org.json.JSONObject;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FavouriteChannel {



  public static void main(String[] args) throws Exception {

    DesiredCapabilities caps = new DesiredCapabilities();
    caps.setCapability("browser", "chrome");
    caps.setCapability("browserstack.debug", "true");
    caps.setCapability("build", "Proximus");
   
    WebDriver driver = new RemoteWebDriver(new URL(ProjectConstants.URL), caps);

    //Retrieving the session id for accessing the logs
    SessionId session = ((RemoteWebDriver)driver).getSessionId();
    String sessionId = session.toString();
    
    //Loading the Proximus URL
    System.out.println("Step 1");
    System.out.println("------");
    System.out.println("Loading the Proximus url");
    driver.get("http://www.proximustv.be/fr");
    @SuppressWarnings("unused")
	WebDriverWait driverWait = new WebDriverWait(driver, 2);
    Assert.assertEquals(true,driver.findElement(By.id(ProjectConstants.URL_CHECK)).isDisplayed());
    System.out.println("\nProximus web page loaded successfully");
    
    System.out.println();
    
    //Maximizing he window
   System.out.println("Step 2");
   System.out.println("------");
   System.out.println("Maximizing the window");
    driver.manage().window().maximize();
   driverWait = new WebDriverWait(driver, 5);
   System.out.println("\nWindow maximized");
    // Thread.sleep(2000);
    
    System.out.println();
    
    //Selecting the Programme TV option
    System.out.println("Step 3");
    System.out.println("------");
    System.out.println("To select the Programme TV option");
    driver.findElement(By.xpath(ProjectConstants.PROGTV_XPATH)).click();
  //  Thread.sleep(5000);
    driverWait = new WebDriverWait(driver, 5);
    Assert.assertEquals(true, driver.findElement(By.id(ProjectConstants.EPG_GRID_ID)).isDisplayed());
    System.out.println("\n'Programme TV' option clicked");
    
    System.out.println();
    
    //Selecting the channel that is to be added to the Favorites
    System.out.println("Step 4");
    System.out.println("------");
    System.out.println("To select a channel that is to be added into Favorites");
    driver.findElement(By.xpath(ProjectConstants.CHANNEL_1)).click();
   // Thread.sleep(5000);
    driverWait = new WebDriverWait(driver, 5);
    Assert.assertEquals(true, driver.findElement(By.cssSelector(ProjectConstants.CSS_FOR_FAVORITE)).isDisplayed());
    System.out.println("\nFavorite icon of the first channel clicked");
    
    System.out.println();
    
    //Selecting dropdown option
    System.out.println("Step 5");
    System.out.println("------");
    System.out.println("Selecting the dropdown option to navigate to Favorites list");
    driver.findElement(By.xpath(ProjectConstants.FAV_DROPDOWN)).click();
  //  Thread.sleep(5000);
    driverWait = new WebDriverWait(driver, 5);
    Assert.assertEquals(true, driver.findElement(By.cssSelector(ProjectConstants.FAV_DROPDOWN_OPEN)).isDisplayed());
    System.out.println("\nDrop down selected successfully");
    
    
    System.out.println();
    
    //Selecting 'favorite' option form the drop down
    
    System.out.println("Step 6");
    System.out.println("------");
    System.out.println("To select the Favorite option from dropdown");
    driver.findElement(By.xpath(ProjectConstants.FAV_OPTION)).click();
    Thread.sleep(5000);
   String optionFromDropdown=driver.findElement(By.cssSelector(ProjectConstants.FAV_DISPLAYED)).getText();
   Assert.assertEquals("Favorites", optionFromDropdown);
    System.out.println("\n'Favorite' option selected from the dropdown");
    System.out.println();
    
    //Checking if the channel is present in the Favorites list
    System.out.println("Step 7");
    System.out.println("------");
    System.out.println("Checking if the channel is present in Favorites list");
    WebElement favoriteElement = driver.findElement(By.xpath(ProjectConstants.CHANNEL_IN_FAV));
    Assert.assertEquals(true, favoriteElement.isDisplayed());
  //  Thread.sleep(5000);
    driverWait = new WebDriverWait(driver, 5);
    System.out.println("\nThe channel marked as favorite is present in the favorite list");
    System.out.println();
    System.out.println("Successfully executed the test case");
    
    driver.quit();
    
    //Retrieving the logs
    
    String command = "curl -u "+ProjectConstants.USERNAME+":"+ProjectConstants.AUTOMATE_KEY+" https://www.browserstack.com/automate/sessions/"+sessionId+".json";

    Process proc = Runtime.getRuntime().exec(command);

    // Read the output

    BufferedReader reader =  
          new BufferedReader(new InputStreamReader(proc.getInputStream()));
    JSONObject json=null;
    String line = "";
    while((line = reader.readLine()) != null) {
        //System.out.print(line + "\n");
    json  = new JSONObject(line);
        
     
    }
String logUrl=json.getJSONObject("automation_session").getString("logs");


System.out.println();


String logCommand = "curl -u "+ProjectConstants.USERNAME+":"+ProjectConstants.AUTOMATE_KEY+" "+logUrl;


 proc = Runtime.getRuntime().exec(logCommand);

// Read the output

BufferedReader reader2 =  
      new BufferedReader(new InputStreamReader(proc.getInputStream()));
String mainResponse="";
while((line = reader2.readLine()) != null) {
 //   System.out.print(line + "\n");
    mainResponse+=line+"\n";
  //  System.out.println("");
}
System.out.println("Screenshots for the above steps:");
System.out.println("-------------------------------");

String[] array=mainResponse.toString().split("\n");
int i=0;
for (int j=0;j<array.length;j++){	
	
	String[] split_data = array[j].split(" ");
	if ((split_data.length) >= 4 && split_data[2].equals("DEBUG")){
		i=i+1;
		
	String 	name = "screenshot"+i+".jpeg";
	File f = new File("/Users/sanjay/Desktop/"+name);
				//f = open((name),'wb')

URL url = new URL(split_data[3]);
InputStream is = url.openStream();
OutputStream os = new FileOutputStream(f);

byte[] b = new byte[2048];
int length;

while ((length = is.read(b)) != -1) {
    os.write(b, 0, length);
}

is.close();
os.close();
				
				System.out.println("Screenshot for step "+i+" saved as "+name);
				System.out.println();
}}
  proc.waitFor();   
    
	    
  }
}

;
package demo;

public class ProjectConstants {
	  public static final String USERNAME = "aravind95";
	  public static final String AUTOMATE_KEY = "cRwY5k4piZJWLDqep78t";
	  public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
	  public static final String PROXIMUS_URL = "http://www.proximustv.be/fr";
	  public static final String URL_CHECK="main-container";
	  public static final String PROGTV_XPATH=".//*[@id='header-slider']/ul/li[2]/a";
	  public static final String EPG_GRID_ID="pxgrid";
	  public static final String CHANNEL_1=".//*[@id='pxgrid']/div[2]/div[4]/div[2]/div[1]/div/div/div[1]/div[1]/ul/li[2]";
	  public static final String CSS_FOR_FAVORITE="div[class='pxchannel pxchannel--favourite']";
	  public static final String FAV_DROPDOWN=".//*[@id='pxgrid']/div[2]/div[4]/div[1]/div[1]/div/button";
	  public static final String FAV_DROPDOWN_OPEN="div[class='pxchannel-filter js-pxchannel-filter pxselect pxselect--open']";
	  public static final String FAV_OPTION =".//*[@id='pxgrid']/div[2]/div[4]/div[1]/div[1]/div/div/div[2]";
	  public static final String FAV_DISPLAYED="button[class='pxselect-selection']";
	  public static final String CHANNEL_IN_FAV=".//*[@id='pxgrid']/div[2]/div[4]/div[2]/div[1]/div/div/div[1]";
	  
	  
	  // J1583001BLACK_062
	  public static final String HUB_PART=".//*[@id='main-container']/div[7]/div[1]/div[1]";
	  public static final String HELP_ICON=".//*[@id='main-container']/div[7]/div[1]/div[1]/div[1]/div/div/span/a/span";
	  public static final String FAQ_PAGE=".//*[@id='463e88db-205f-4f28-96b2-9357a8259e8e']";

	  // J1583001BLACK_035
	  public static final String CTRL_BAR=".//*[@id='main-container']/div[7]/div[1]/div[1]/div[1]/div/div/div[7]/div[2]";
	  public static final String MOVE_CURSOR=".//*[@id='header-slider']/ul/li[3]/div/a";

	  
}



package demo;

import java.net.MalformedURLException;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.ui.WebDriverWait;

public class J1583001BLACK_062 extends BrowserstackTestNGTest {

	  public WebDriverWait driverWait = null;  
	    
	@org.testng.annotations.Test
	    public void loadProximusUrl() throws MalformedURLException, InterruptedException{
	   
	    // Retrieving the session id for accessing the logs
	   	    
	    // Loading the Proximus URL
	    System.out.println("Step 1");
	    System.out.println("------");
	    System.out.println("Loading the Proximus url");
		driver.get(ProjectConstants.PROXIMUS_URL);
	    driverWait = new WebDriverWait(driver, 2);
	    Assert.assertTrue(driver.findElement(By.id(ProjectConstants.URL_CHECK)).isDisplayed());
	    System.out.println("\nProximus web page loaded successfully\n");
	   
	    // Maximizing window
	    System.out.println("Step 2");
		System.out.println("------");
		System.out.println("Maximizing the window");
		driver.manage().window().maximize();
		driverWait = new WebDriverWait(driver, 5);
		System.out.println("\nWindow maximized\n");
		Assert.assertTrue(true);
	   
		// Checking the user able to see the HUB page
		System.out.println("Step3");
		System.out.println("------");
	    System.out.println("Checking the user able to see the HUB page");
	    WebElement aa2 = driver.findElement(By.xpath(ProjectConstants.HUB_PART));
	    Assert.assertEquals(true, aa2.isDisplayed());
	    Thread.sleep(5000);
	    
	    // Checking the HELP button displayed on hub page
	    System.out.println("\nStep4");
	    System.out.println("------");
	    System.out.println("Checking the HELP button displayed on hub page");
	    WebElement aa3 = driver.findElement(By.xpath(ProjectConstants.HELP_ICON));
	    Assert.assertEquals(true, aa3.isDisplayed());
	    Thread.sleep(5000);
	    
	    // Checking the user able to select HELP button
	    System.out.println("\nStep5");
	    System.out.println("------");
	    System.out.println("Checking the user able to select HELP button");
	    driver.findElement(By.xpath(ProjectConstants.HELP_ICON)).click();
	    Thread.sleep(10000);
	    
	    //  Checking the user provided with FAQ page on clicking Help button
	    System.out.println("\nStep6");
	    System.out.println("------");
	    System.out.println("Checking the user provided with FAQ page on clicking Help button");
	    
	    // Switch to FAQ page
	    for(String winHandle : driver.getWindowHandles()){
	        driver.switchTo().window(winHandle);
	    }
	    
	    WebElement aa4 = driver.findElement(By.xpath(ProjectConstants.FAQ_PAGE));
	    Assert.assertEquals(true, aa4.isDisplayed());
	    Thread.sleep(5000);
	    
	    System.out.println("\nSuccessfully executed the test case");
		 
	    }
	
}

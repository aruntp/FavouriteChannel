package demo;

import java.net.MalformedURLException;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class J1583001BLACK_035 extends BrowserstackTestNGTest {
	
	@org.testng.annotations.Test
    public void loadProximusUrl() throws MalformedURLException, InterruptedException{
		
		// Loading the Proximus URL
	    System.out.println("Step 1");
	    System.out.println("------");
	    System.out.println("Loading the Proximus url");
		driver.get("http://www.proximustv.be/fr");
	    System.out.println("\nProximus web page loaded successfully\n");
	    
	    // Maximizing window
	    driver.manage().window().maximize();
	    Thread.sleep(5000);
	    
	    // Checking the Control bar is displayed
	    System.out.println("Step 2");
	    System.out.println("------");
	    System.out.println("Checking the Control bar is displayed");
	    WebElement aa1 = driver.findElement(By.xpath(ProjectConstants.CTRL_BAR));
	    Assert.assertEquals(true, aa1.isDisplayed());
	    
	    // Checking the Control bar is displayed on hovering mouse over the player
	    System.out.println("\nStep 3");
	    System.out.println("------");
	    System.out.println("Checking the Control bar is displayed on hovering mouse over the player");
	    Actions actions = new Actions(driver);
	    WebElement mainMenu = driver.findElement(By.xpath(ProjectConstants.CTRL_BAR));
	    actions.moveToElement(mainMenu).build().perform();
//	    WebElement Mainmenu=driver.findElement(By.xpath(ProjectConstants.CTRL_BAR));
//	    String strJavaScript = "var element = arguments[0];"
//	            + "var mouseEventObj = document.createEvent('MouseEvents');"
//	            + "mouseEventObj.initEvent( 'mouseover', true, true );"
//	            + "element.dispatchEvent(mouseEventObj);";
//	    JavascriptExecutor js =  (JavascriptExecutor) driver;
//	    js.executeScript(strJavaScript, Mainmenu);
	    Thread.sleep(5000);
	    WebElement aa2 = driver.findElement(By.xpath(ProjectConstants.CTRL_BAR));
	    Assert.assertEquals(true, aa2.isDisplayed());    
	    
	    // Checking the Control bar disappears on moving the mouse away from the player
	    System.out.println("\nStep 4");
	    System.out.println("------");
	    System.out.println("Checking the Control bar disappears on moving the mouse away from the player");
	    // Moving the cursor
	    Actions actions1 = new Actions(driver);
	    WebElement mainMenu1 = driver.findElement(By.xpath(ProjectConstants.MOVE_CURSOR));
	    actions1.moveToElement(mainMenu1).perform();
//	    WebElement Mainmenu1=driver.findElement(By.xpath(ProjectConstants.MOVE_CURSOR));
//	    String strJavaScript1 = "var element = arguments[0];"
//	            + "var mouseEventObj = document.createEvent('MouseEvents');"
//	            + "mouseEventObj.initEvent( 'mouseover', true, true );"
//	            + "element.dispatchEvent(mouseEventObj);";
//	    JavascriptExecutor js1 =  (JavascriptExecutor) driver;
//	    js1.executeScript(strJavaScript1, Mainmenu1);
	    Thread.sleep(5000);
	    WebElement aa3 = driver.findElement(By.xpath(ProjectConstants.CTRL_BAR));
	    Assert.assertEquals(false, aa3.isDisplayed());
	    
	    System.out.println("\nSuccessfully executed the test case");
	    
	}

//	  public static void main(String[] args) throws Exception {
//	    
//	  }
}

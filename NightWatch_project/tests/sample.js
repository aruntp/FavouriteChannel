module.exports = {
  'Demo test Google' : function (browser) {
    browser
      .windowMaximize()
      .url('http:192.168.0.105:9080/Sample/login.html')
      .waitForElementVisible('input[type=text]', 4000)
      .setValue('input[type=text]', 'user1')
      .waitForElementVisible('input[type=password]', 4000)
      .setValue('input[type=password]', 'abcd123')	
      .waitForElementVisible('button[type=submit]', 4000)
      .click('button[type=submit]')
      .url('http:192.168.0.105:9080/Sample/index.html')	
      .waitForElementVisible('select[id= genredropdown]', 4000)
      .click('select[id= genredropdown]')
      .execute('document.getElementById("genredropdown").options[1].selected=true')
      .click('select[id= genredropdown]')
      browser.click('select[id="genredropdown"] option[value="1"]')
      .keys(['\uE016'])	
      .pause(10000)
      .end();
  }
};



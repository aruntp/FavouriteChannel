module.exports = {
  'Beta Proximus': function( _browser ) {
    _browser
    .url('http://beta.proximustv.be/')
    .pause(10000)
    .useXpath().click(".//*[@id='main-container']/div/div/div/div[1]/div/div/ul/li/div/div/div/div/a")
    .pause(50000)
    .useCss()
    .waitForElementVisible("//a[text()='français']", 2000)
    .click("//a[text()='français']")
    .pause(4000)
    .end();

  }
}
module.exports = {
	'ProximusTV_TestCase_To see a Help button': function( _browser ) {
    	_browser
    	.windowMaximize()
	
	// Loading the Proximus URL
    	.url('http://www.proximustv.be/fr')

	// Checking the user able to see the HUB page
	.assert.elementPresent("div[class=content-wrapper")

	// Checking the HELP button displayed on hub page
	.assert.elementPresent("span[class=pxplayer-help")

	// Checking the user able to select HELP button
	.useXpath()
    	.click(".//*[@id='main-container']/div[7]/div[1]/div[1]/div[1]/div/div/span/a/span")

	//  Checking the user provided with FAQ page on clicking Help button
	// Switch to FAQ page
	_browser.window_handles(function(result) {
     	var handle = result.value[1];
     	_browser.switchWindow(handle);
   	});
	_browser.pause(5000)
	.assert.elementPresent(".//*[@id='463e88db-205f-4f28-96b2-9357a8259e8e']/div[5]/div/div/ul/li[3]/div/div/ul/li[1]")
    	
	.end();

  }
}
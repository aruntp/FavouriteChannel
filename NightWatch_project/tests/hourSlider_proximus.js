module.exports = {
  'Proximus TV Test Case hour slider': function( _browser ) {
    _browser
    .windowMaximize()
    .url('http://www.proximustv.be/fr')
    .useXpath().click(".//*[@id='main-container']/div/div/div/div[2]/div/div/ul/li/div/div/div/div/a")
    .pause(10000)
    .useXpath().click(".//*[@id='header-slider']/ul/li[2]/a")
    .useXpath().click(".//*[@id='tvtab-grid']/div[1]/div[3]/ul/li[7]")
    .pause(4000)
    .end();

  }
}
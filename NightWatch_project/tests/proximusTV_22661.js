module.exports = {
  'Proximus TV TestId: 22661': function( _browser ) {
    _browser
    .windowMaximize()
    .url('http://www.proximustv.be/fr')
    .waitForElementVisible('li[data-navtag=proximustv-text]', 10000)
    .waitForElementVisible('div[id=header-slider]', 1000)
    .useXpath().click(".//*[@id='header-slider']/ul/li[2]/div/a")
.useCss()
.pause(2000)
    .elements('css selector','.prog.prog-future', function (result) { 
	for (var i = 0; i < result.value.length; i++) {this.click(result) } })

    .end();
  }
}
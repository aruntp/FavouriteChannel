module.exports = {
	'ProximusTV_Test Case_To see the controls bar appearing and disappearing on hover': function( _browser ) {
    	_browser
    	.windowMaximize()
	
	// Loading the Proximus URL
    	.url('http://www.proximustv.be/fr')

	// Checking the Control bar is displayed
    	.waitForElementVisible('div[class=pxplayer__controls-wrapper]', 1000)

	// Checking the Control bar is displayed on hovering mouse over the player
	.moveToElement('div[class=pxplayer__controls-wrapper]', 10, 10)
	.waitForElementVisible('div[class=pxplayer__controls-wrapper]', 1000)

	// Checking the Control bar disappears on moving the mouse away from the player
	.moveToElement('div[class=mh__dropdown-toggle]', 10, 10)
	.waitForElementNotVisible('div[class=pxplayer__controls-wrapper]', 1000)

    	.end();

  }
}